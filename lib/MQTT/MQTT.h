#ifndef MQTT_H
#define MQTT_H

#include <Client.h>
#include <PubSubClient.h>

#ifdef ESP8266
#include <functional>
#define MQTT_PAYLOAD_CALLBACK_SIGNATURE std::function<void(String, String)> payloadCallback
#else
#define MQTT_PAYLOAD_CALLBACK_SIGNATURE void (*payloadCallback)(String, String)
#endif

class MQTT : public PubSubClient {
public:
    MQTT(Client&, const char*, uint16_t);
    MQTT(Client&, const char*, uint16_t, MQTT_PAYLOAD_CALLBACK_SIGNATURE);
    MQTT(Client&, const char*, uint16_t, const char*);
    MQTT(Client&, const char*, uint16_t, const char*, MQTT_PAYLOAD_CALLBACK_SIGNATURE);
    void setPayloadCallback(MQTT_PAYLOAD_CALLBACK_SIGNATURE);
    void setClientId(const char*);
    const char* getClientId();
    bool connect();
protected:
    void setupCallback();
private:
    const char* clientId = "sensor_module";
    MQTT_PAYLOAD_CALLBACK_SIGNATURE;
};

#endif
