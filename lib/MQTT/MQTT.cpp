#include "MQTT.h"

MQTT::MQTT(Client& net, const char* host, uint16_t port) : PubSubClient (host, port, net) {
    setupCallback();
}

MQTT::MQTT(Client& net, const char* host, uint16_t port, MQTT_PAYLOAD_CALLBACK_SIGNATURE) : PubSubClient (host, port, net) {
    setupCallback();
    setPayloadCallback(payloadCallback);
}

MQTT::MQTT(Client& net, const char* host, uint16_t port, const char* clientId) : MQTT (net, host, port) {
    setClientId(clientId);
}

MQTT::MQTT(Client& net, const char* host, uint16_t port, const char* clientId, MQTT_PAYLOAD_CALLBACK_SIGNATURE) : MQTT (net, host, port, payloadCallback) {
    setClientId(clientId);
}

void MQTT::setPayloadCallback(MQTT_PAYLOAD_CALLBACK_SIGNATURE) {
    this->payloadCallback = payloadCallback;
}

void MQTT::setClientId(const char* clientId) {
    this->clientId = clientId;
}

const char* MQTT::getClientId() {
    return clientId;
}

void MQTT::setupCallback() {
    setCallback([this] (char* topic, byte* payload, unsigned int length) {
        payload[length] = '\0';
        String payload_str = String((char*)payload);
        String topic_str = String(topic);

        if (this->payloadCallback) {
            this->payloadCallback(topic_str, payload_str);
        }
    });
}

bool MQTT::connect() {
    return PubSubClient::connect(this->clientId);
}
