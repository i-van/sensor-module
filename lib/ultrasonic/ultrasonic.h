#ifndef ultrasonic_h
#define ultrasonic_h

#include "Arduino.h"

class Ultrasonic {
public:
  Ultrasonic(int trigPin, int echoPin);
  double detectDistance(int delay = 10);
private:
  int _trigPin, _echoPin;
};

#endif
