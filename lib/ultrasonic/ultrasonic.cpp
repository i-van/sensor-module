#include "ultrasonic.h"

Ultrasonic::Ultrasonic(int trigPin, int echoPin) : _trigPin{ trigPin }, _echoPin{ echoPin } {
  pinMode(_trigPin, OUTPUT);
  pinMode(_echoPin, INPUT);
}

double Ultrasonic::detectDistance(int delay) {
  // Trigger sonic wave
  digitalWrite(_trigPin, LOW);
  delayMicroseconds(delay);
  digitalWrite(_trigPin, HIGH);
  delayMicroseconds(delay);
  digitalWrite(_trigPin, LOW);

  // Detect time and convert to distance
  // TODO generalization to detectDuration
  long duration = pulseIn(_echoPin, HIGH);
  double distance = (duration * 0.034) / 2;

  return distance;
}
