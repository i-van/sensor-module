#ifndef PIR_h
#define PIR_h

#include "Arduino.h"

class PIR {
public:
  PIR(int pin);
  void check();
  int hasMotion();
private:
  int _pir_in, last_state, state;
};

#endif
