#include "PIR.h"

PIR::PIR(int pin) : _pir_in{ pin } {
  pinMode(_pir_in, INPUT);
}

void PIR::check() {
  state = digitalRead(_pir_in);
  if (state == HIGH) {
    // Movement detected
    if (last_state == LOW) {
      last_state = HIGH;
    }
  } else {
    // No movement detected
    if (last_state == HIGH) {
      last_state = LOW;
    }
  }
}

int PIR::hasMotion() {
  return last_state;
}
