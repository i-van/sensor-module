#include "Arduino.h"
#include "LEDRGB.h"

LEDRGB::LEDRGB(int redPin, int greenPin, int bluePin) : _pin{ redPin, greenPin, bluePin } {
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void LEDRGB::enable() {
  analogWrite(_pin[0], _color[0]);
  analogWrite(_pin[1], _color[1]);
  analogWrite(_pin[2], _color[2]);
  _on = 1;
}

void LEDRGB::disable() {
  analogWrite(_pin[0], _mode == COMMON_ANODE ? 255 : 0);
  analogWrite(_pin[1], _mode == COMMON_ANODE ? 255 : 0);
  analogWrite(_pin[2], _mode == COMMON_ANODE ? 255 : 0);
  _on = 0;
}

void LEDRGB::setOn(bool on) {
  on ? enable() : disable();
}

bool LEDRGB::isEnabled() {
  return _on;
}

void LEDRGB::setColor(int red, int green, int blue) {
  analogWrite(_pin[0], (_color[0] = _mode == COMMON_ANODE ? 255 - red : red));
  analogWrite(_pin[1], (_color[1] = _mode == COMMON_ANODE ? 255 - green : green));
  analogWrite(_pin[2], (_color[2] = _mode == COMMON_ANODE ? 255 - blue : blue));
}

void LEDRGB::setMode(int mode) {
  _mode = mode;
}
