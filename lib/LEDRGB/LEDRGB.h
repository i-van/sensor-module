#ifndef LEDRGB_H
#define LEDRGB_H

#include "Arduino.h"
#include "diode.h"

class LEDRGB {
public:
  LEDRGB(int redPin, int greenPin, int bluePin);
  void enable();
  void disable();
  bool isEnabled();
  void setColor(int red, int green, int blue);
  void setMode(int mode);
  void setOn(bool on);
private:
  int _mode = COMMON_CATHODE;
  int _pin[3];
  int _color[3] = {255, 255, 255};
  int _on = 1;
};

#endif
