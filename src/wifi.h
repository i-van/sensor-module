#ifndef WIFI_H
#define WIFI_H

#include <ESP8266WiFi.h>

void setupWifi(const char* SSID, const char* password);

#endif
