#include <Arduino.h>
#include <ArduinoJson.h>
#include <TimedAction.h>
#include <MQTT.h>
#include <ultrasonic.h>
#include <PIR.h>

#include "wifi.h"

// Define string combined
#define STR(x) #x
#define DEVICE_SERIAL_F(name) "Sensor/" DEVICE_SERIAL
#define DEVICE_SERIAL_N DEVICE_SERIAL_F(DEVICE_SERIAL)

// Ultrasonic Trigger PIN
#define U_TRIG D1
// Ultrasonic ECHO PIN
#define U_ECHO D2
// PIR Signal PIN
#define PIR_IN D4

// JSON Serializer
StaticJsonBuffer<200> jsonBuffer;
// WiFi Client (referencing)
WiFiClient wifiClient;
// MQTT Client
void callback(String topic, String payload);
MQTT mqtt(wifiClient, MQTT_SERVER, MQTT_PORT, DEVICE_SERIAL_N, callback);
// Ultrasonic Sensor
Ultrasonic ultrasonic(U_TRIG, U_ECHO);
// PIR Sensor
PIR pir(PIR_IN);

// Controller Action
void detection();
TimedAction action = TimedAction(INTERVAL_CHECK, detection);

/**
 * Serializer to serialize data to JSON format for sending to MQTT Broker
 */
JsonObject& root = jsonBuffer.createObject();
void serializeJSON(double distance, int motion, String& target) {
    root["sensor_module_id"] = String(DEVICE_SERIAL);
    root["ultrasonic"] = distance;
    root["pir"] = (motion == HIGH ? true : false);
    root.printTo(target);
}

/**
 * Setup / Startup
 */
void setup() {
    Serial.begin(DEBUG_BAUD);

    // Disable Action by default
    action.disable();

    // Wait for 1 seconds to ready things
    delay(1000);

    // Setup WiFi connection
    setupWifi(WIFI_SSID, WIFI_PWD);
}

/**
 * Main Loop
 */
void loop() {
    // Connect MQTT if not connected (or lost connection)
    if (!mqtt.connected()) {
        // Loop until we're reconnected
        while (!mqtt.connected()) {
            Serial.println("Attempting MQTT connection...");
            if (mqtt.connect()) {
                // Connect successful
                Serial.println("Successfully connected with MQTT");
                Serial.print("Client: ");
                Serial.println(mqtt.getClientId());
                // Subscribe Topics
                mqtt.subscribe(CENTRAL_TOPIC);
            } else {
                // Fail to connect
                Serial.print("failed, rc=");
                Serial.print(mqtt.state());
                Serial.println(" try again in 5 seconds");
                // Wait 5 seconds before retrying
                delay(5000);
            }
        }
    }
    // Client loop for subscription callback
    mqtt.loop();
    // Detection action check for execute (by its interval setting)
    action.check();
}

void detection() {
    // PIR Motion checking
    pir.check();

    // Get detection value & construct json
    double distance = ultrasonic.detectDistance();
    int motion = pir.hasMotion();

    // Serialize data to JSON
    String json;
    serializeJSON(distance, motion, json);

    // Publish to MQTT
    String topic = String(BASE_TOPIC) + String(DEVICE_ROW);
    mqtt.publish(topic.c_str(), json.c_str());

    Serial.println("Send data to topic = " + topic);
    Serial.println("Detecting : " + json);
}

/**
 * MQTT Callback for subscription
 */
void callback(String topic, String payload) {
    Serial.println("Getting topic: " + topic);
    Serial.println("Getting payload: " + payload);

    if (topic.compareTo(CENTRAL_TOPIC) == 0) {
        if (payload.compareTo("START_DETECTION") == 0) {
            action.enable();
            Serial.println("Start Detection");
        } else if (payload.compareTo("STOP_DETECTION") == 0) {
            action.disable();
            Serial.println("End Detection");
        }
    }
}

// endregion
