# iVan Sensor Module : Forgotten Children in Kindergarten Van Detector

Detect the forgotten children in van using multiple Sensors and Microcontrollers and send the data wirelessly to [Central Module](#) which is the Raspberry Pi that Provide WiFi-Hotspot and Running an Embedded MQTT Broker Server.

This program built by using [Compatible Arduino Core](https://github.com/esp8266/Arduino), Complied and Uploaded via [PlatformIO](https://platformio.org), IDE using [Virtual Studio Code](https://code.visualstudio.com) + [PlatformIO IDE](http://docs.platformio.org/en/latest/ide/vscode.html) extension.

**Recommended to learn the Arduino Framework, C++ and PlatformIO first before modifing the code**

### Specifications
- Device - NodeMCU (Both V2 and V3) and ESP-12
- Main Sensors - PIR (For motion detection), Ultrasonic (For distance detection)
- Support Sensors - DHT11 (Humidity and Temparature Sensor)

### Settings (in platformio.ini)
#### Arguments
- `device_name = "NodeMCU 1.0 (V3)"` - Specify the device's name.
- `device_serial = "-7KMySEelABCD1"` - Specify the device's serial id.
- `module_row = 1` - Which row will this module installed.
- `interval_check = 1000` - Interval time to detect and send the data.
- `wifi_ssid = "Pi3-AP-IoT"` - SSID to connect to Central Module.
- `wifi_pwd = "raspberry"` - Password to connect to Central Module.
- `mqtt_host = "mqtt.pi"` - MQTT Host to connect to MQTT Server in Central Module.
- `mqtt_port = 1883` - MQTT Port to connect to MQTT Server in Central Module.
- `debug_baud = 9600` - Debugging Serial baudwidth.
#### Default Settings (Common) (*Don't modify these unless you know what are you doing :)*)
- `base_topic = "seat/row/"` - Base topic which device will send the sensors' data.
- `central_topic = "seat/row/central"` - Central topic which device will listen the command.

### How to build and upload to NodeMCU
1. Config the environment of the Device in `platformio.ini`
2. Config the arguments and settings in `platformio.ini`
3. Compile by execute `platformio run` in cmd or using `Build` in PlatformIO IDE
4. Upload (flash) by execute `platformio upload` in cmd or using `Upload` in PlatformIO IDE

**Note:** If you connect to multiple device, you need to specify `upload_port` in `platformio.ini`

### Schematics
Coming Soon

**Create and Maintain by Chaniwat Seangchai (chaniwat.meranote@gmail.com) & Achiraya Songput (achiraya.songput@gmail.com)**
